package com.pragma.powerup.messengermicroservice.domain.usecase;

import com.pragma.powerup.messengermicroservice.domain.api.IMessageServicePort;
import com.pragma.powerup.messengermicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.messengermicroservice.domain.model.Message;
import com.twilio.Twilio;
import org.springframework.util.ObjectUtils;


public class MessageUseCase implements IMessageServicePort {


    @Override
    public Message sendMessage(Message message) {

        if (isEmpty(message.getName())) {
            throw new ValidationException("empty name");
        } else if (!isNameValid(message.getName())) {
            throw new ValidationException("invalid name");
        }

        if (isEmpty(message.getPhone())) {
            throw new ValidationException("empty phone");
        } else if (!isPhoneValid(message.getPhone())) {
            throw new ValidationException("invalid phone");
        }

        Twilio.init("ACb6a8f495e3d11fac8f03e0b82f89d8b3", "b8a24037eef2ebc578b50a13a6b9c0b2");
        com.twilio.rest.api.v2010.account.Message.creator(
                        new com.twilio.type.PhoneNumber(message.getPhone()),
                        "MG2e1ff6bb241ece0b654434d32610951f",
                        "Hello " + message.getName() +
                                " your order is ready!, claim with this pin: "
                                + message.getPin() + ".")

                .create();

        return message;
    }


    //validacion phone: numeros menores que 13 digitos y mayores a 7 digitos, debe permiter el +
    private boolean isPhoneValid(String phone) {
        return phone.matches("^[+]?(\\d){7,12}$");

    }


    private boolean isEmpty(Object field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return ObjectUtils.isEmpty(field);
    }

    private boolean isNameValid(String name) {
        return name.matches("^[0-9a-zA-Z]*$") && !(name.matches("^[\\d]*$"));
    }


}
