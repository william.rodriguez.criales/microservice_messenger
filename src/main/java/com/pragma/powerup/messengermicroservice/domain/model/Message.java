package com.pragma.powerup.messengermicroservice.domain.model;

public class Message {
    private String name;
    private String phone;
    private String pin;

    public Message(String name, String phone, String pin) {
        this.name = name;
        this.phone = phone;
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
