package com.pragma.powerup.messengermicroservice.domain.exceptions;

public class ValidationException extends RuntimeException{

    public ValidationException(String message) {
        super(message);
    }
}
