package com.pragma.powerup.messengermicroservice.domain.api;

import com.pragma.powerup.messengermicroservice.domain.model.Message;

public interface IMessageServicePort {
    Message sendMessage(Message message);

}
