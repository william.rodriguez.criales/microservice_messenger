package com.pragma.powerup.messengermicroservice.configuration;

import com.pragma.powerup.messengermicroservice.domain.api.IMessageServicePort;
import com.pragma.powerup.messengermicroservice.domain.usecase.MessageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    @Bean
    public IMessageServicePort restaurantServicePort() {
        return new MessageUseCase();
    }



}
