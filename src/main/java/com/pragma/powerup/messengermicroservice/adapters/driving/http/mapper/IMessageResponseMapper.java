package com.pragma.powerup.messengermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.messengermicroservice.adapters.driving.http.dto.response.MessageResponseDto;
import com.pragma.powerup.messengermicroservice.domain.model.Message;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IMessageResponseMapper {

    MessageResponseDto toMessageResponseDto(Message message);
/*
    @Mapping(source = "person.name", target = "name")
    @Mapping(source = "person.surname", target = "surname")
    @Mapping(source = "person.mail", target = "mail")
    @Mapping(source = "person.phone", target = "phone")
    @Mapping(source = "person.address", target = "address")
    @Mapping(source = "person.idDniType", target = "idDniType")
    @Mapping(source = "person.dniNumber", target = "dniNumber")
    @Mapping(source = "person.idPersonType", target = "idPersonType")
    PersonResponseDto userToPersonResponse(User user);
    List<PersonResponseDto> userListToPersonResponseList(List<User> userList);


    PersonResponseDto toPersonResponseDto(Person person);
*/
}


