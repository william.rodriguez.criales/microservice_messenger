package com.pragma.powerup.messengermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.messengermicroservice.adapters.driving.http.dto.response.MessageResponseDto;

public interface IMessageHandler {
    MessageResponseDto sendMessage(String name, String phone, String pin);

}
