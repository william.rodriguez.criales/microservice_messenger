package com.pragma.powerup.messengermicroservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MessageResponseDto {
    private String name;
    private String phone;

}
