package com.pragma.powerup.messengermicroservice.adapters.driving.http.handlers.impl;


import com.pragma.powerup.messengermicroservice.adapters.driving.http.dto.response.MessageResponseDto;

import com.pragma.powerup.messengermicroservice.adapters.driving.http.handlers.IMessageHandler;

import com.pragma.powerup.messengermicroservice.adapters.driving.http.mapper.IMessageRequestMapper;
import com.pragma.powerup.messengermicroservice.adapters.driving.http.mapper.IMessageResponseMapper;

import com.pragma.powerup.messengermicroservice.domain.api.IMessageServicePort;

import com.pragma.powerup.messengermicroservice.domain.model.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageHandlerImpl implements IMessageHandler {
    private final IMessageServicePort messageServicePort;
    private final IMessageRequestMapper messageRequestMapper;
    private final IMessageResponseMapper messageResponseMapper;

    @Override
    public MessageResponseDto sendMessage(String name, String phone, String pin) {
        Message message = messageServicePort.sendMessage(messageRequestMapper.toMessage(name, phone, pin));
        return messageResponseMapper.toMessageResponseDto(message);
    }



}
