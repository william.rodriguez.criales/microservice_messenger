package com.pragma.powerup.messengermicroservice.adapters.driving.http.controller;


import com.pragma.powerup.messengermicroservice.adapters.driving.http.dto.request.MessageRequestDto;
import com.pragma.powerup.messengermicroservice.adapters.driving.http.dto.response.MessageResponseDto;
import com.pragma.powerup.messengermicroservice.adapters.driving.http.handlers.IMessageHandler;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.pragma.powerup.messengermicroservice.configuration.Constants.TWILIO_ACCOUNT_SID;
import static com.pragma.powerup.messengermicroservice.configuration.Constants.TWILIO_AUTH_TOKEN;
import static com.twilio.example.ValidationExample.ACCOUNT_SID;
import static com.twilio.example.ValidationExample.AUTH_TOKEN;

import com.twilio.converter.Promoter;


import java.net.URI;
import java.math.BigDecimal;

@RestController
@RequestMapping("/message")
@RequiredArgsConstructor
//@SecurityRequirement(name = "jwt")
public class MessageRestController {
    private final IMessageHandler messageHandler;

    @Operation(summary = "send message",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Message send",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map")))
            })


    @PostMapping()
    public ResponseEntity<MessageResponseDto> sendMessage(@RequestParam ("name") String name, @RequestParam ("phone")String phone, @RequestParam("pin") String pin) {

        MessageResponseDto messageResponseDto = messageHandler.sendMessage(name, phone, pin);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(messageResponseDto);

    }

}



