package com.pragma.powerup.messengermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MessageRequestDto {
    private String name;
    private String phone;

}
