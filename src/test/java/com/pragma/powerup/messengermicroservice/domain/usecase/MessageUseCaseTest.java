package com.pragma.powerup.messengermicroservice.domain.usecase;

import com.pragma.powerup.messengermicroservice.domain.exceptions.ValidationException;
import com.pragma.powerup.messengermicroservice.domain.model.Message;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;


@ExtendWith(SpringExtension.class)
//@ExtendWith(MockitoExtension.class)
class MessageUseCaseTest {

    @InjectMocks
    MessageUseCase messageUseCase;

    @Test
    void saveMessageValidationsEmptyName() {
        //arrange
        Message message = new Message("", "", "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> messageUseCase.sendMessage(message));
        //assert
        assertEquals("empty name", validationException.getMessage());
        //verify(iDishPersistencePort, never()).updateDish(dish);
    }

    @Test
    void saveMessageValidationsInvalidName() {
        //arrange
        Message message = new Message("2222", "", "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> messageUseCase.sendMessage(message));
        //assert
        assertEquals("invalid name", validationException.getMessage());
    }

    @Test
    void saveMessageValidationsEmptyPhone() {
        //arrange
        Message message = new Message("will", "", "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> messageUseCase.sendMessage(message));
        //assert
        assertEquals("empty phone", validationException.getMessage());
    }

    @Test
    void saveMessageValidationsInvalidPhone() {
        //arrange
        Message message = new Message("will", "33333", "");
        //act
        ValidationException validationException = assertThrows(ValidationException.class, () -> messageUseCase.sendMessage(message));
        //assert
        assertEquals("invalid phone", validationException.getMessage());
    }

    @Test
    void saveMessageSuccessful() {
        //arrange
        Message message = new Message("will", "+573023826662", "");
        //act
        messageUseCase.sendMessage(message);
      }

}
